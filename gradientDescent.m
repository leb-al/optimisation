function [ solution, interationCount ] = gradientDescent (f, fGrad, start, eps, lambda)
  interationCount = 0;
  current = start;
  notoptimal=true;
  while (notoptimal)
    ++interationCount;
    grad = fGrad(current);
    problemsWithLambda = true;
    fCurrent = f(current);
    while (problemsWithLambda)
      next = current - lambda * grad;
      fNext = f(next);
      if ((isfinite(fNext) == 0 ))
        lambda /= 2;
      else
        problemsWithLambda=false;
      endif;
    endwhile;
    if (abs(fCurrent - fNext) < eps)
      solution = next;
      notoptimal=false;
    endif;
    current = next;
  endwhile;
endfunction;