#task 123a
#Lebedev Aleksey, 393

global dimension = 2;
global halfn=25;
global n = 2*halfn;
global fileX = zeros(n,dimension);
global fileY = ones(1,n);
function [result] = Q(w,x,y)
  sizeArr = size(x);
  m=sizeArr(2);
  n=sizeArr(1);
  extendedX=[-ones(n,1) x];
  tmp = extendedX*w';
  tmp =-diag(y)*tmp; 
  tmp = exp(tmp)+1;
  tmp = log(tmp);
  result = ones(1,n)*tmp;
  
endfunction

function [gradient] = QGradient(w,x,y)
  sizeArr = size (x);
  m=sizeArr(2);
  n=sizeArr(1);
  extendedX=[-ones(n,1) x];
  tmp = extendedX*w';
  tmp =-diag(y)*tmp; 
  tmp = exp(tmp);
  sumElements = diag(-y)*diag(tmp)*(1\(tmp+1));
  gradient = sumElements'*extendedX;
endfunction

function [gradient] = QGessian(w,x,y)
  sizeArr = size (x);
  m=sizeArr(2);
  n=sizeArr(1);
  extendedX=[-ones(n,1) x];
  tmp = extendedX*w';
  tmp =-diag(y)*tmp; 
  tmp = exp(tmp);
  sumElements = diag(y)*diag(y)*diag(tmp);
  sumElements=sumElements*diag(1\(tmp+1))*(1\(tmp+1));
  gradient = extendedX'*diag(sumElements)*extendedX;
endfunction

function res = fQ(w)
  global fileX;
  global fileY;
  res = Q(w,fileX,fileY);
endfunction

function res = fQGrad(w)
  global fileX;
  global fileY;
  res = QGradient(w,fileX,fileY);
endfunction

function res = fQGessian(w)
  global fileX;
  global fileY;
  res = QGessian(w,fileX,fileY);
endfunction

function res = classificator(x,w)
  res = sign([-1 x]*w');
endfunction

function [iterationEps, logEps] = countStepNumberNewton (eps, begin)
  cureps = 1;
  run = true;
  logEps=[];
  iterationEps=[];
  while(run)
    if cureps<eps
      cureps=eps;
      run = false;
    endif
    [w,i] = newton(@fQGrad,@fQGessian,begin,cureps);
    logEps= [logEps -log(cureps)];
    iterationEps= [iterationEps i];
    cureps/=3;
  endwhile
endfunction         

function [w,iterations,logEps,iterationEps] = goodGradient (f,
         fGrad, eps, lambda, begin)
  cureps = 1;
  run = true;
  w = begin;
  iterations=0; 
  logEps=[];
  iterationEps=[];
  while(run)
    if cureps<eps
      cureps=eps;
      run = false;
    endif
    [w,i] = gradientDescent(f,fGrad,w,cureps,lambda);
    iterations+=i;
    logEps= [logEps -log(cureps)];
    iterationEps= [iterationEps iterations];
    cureps/=3;
    lambda*=1.2;
  endwhile
endfunction

function [good,bad] = movingContol(indexes)
  global fileX;
  global fileY;
  global dimension;
  x=fileX;
  y=fileY;
  controlX=[];
  controlY=[];
  newX = [];
  newY = [];
  for i=1:size(fileY)(2)
    if(indexes(i)==1)
      newX=[newX;x(i,1:dimension)];
      newY=[newY y(i)];
    else
      controlX=[controlX;x(i,1:dimension)];
      controlY=[controlY; y(i)];
   endif;
  endfor;
  good=0;
  bad=0;
  [w,i]=newton(@fQGrad,@fQGessian,zeros(1,dimension+1),1E-5);
  for i=1:size(controlY)
    if(controlY(i)==classificator(controlX(i,1:dimension),w))
      good+=1;
    else
      bad+=1;
    endif;  
  endfor;
  global fileX = newX;
  global fileY = newY;
  global fileX=x;
  global fileY=y;
endfunction


function runMovingContol()
  global fileY;
  parts = 5;
  sizeOfPart = floor(length(fileY) / parts);
  for i = 1:parts
    partFirst = sizeOfPart * (i - 1) + 1;
    partLast = partFirst + sizeOfPart - 1;
    config = ones(1,size(fileY)(2));
    config(partFirst:partLast)=zeros(1,sizeOfPart);
    printf("control %d\n",i);
    [g,b]=movingContol(config);
    total=(g+b)/100.0;
    printf("good %d (%f); bad %d (%f); total %d %d\n", 
            g, g/total, b, b/total, g+b, sizeOfPart);
  endfor;  
endfunction;

function work(isSeaprate)
  global dimension;
  global n;
  global halfn;
  global fileX;
  global fileY;
  
  figDots = figure();
  plot(fileX((1+halfn):n,1),fileX(1:halfn,2),"*;-1;",
        fileX((halfn+1):n,1),fileX((halfn+1):n,2),"*;1;");
  title("Visual control");
  
  [e,ie] = countStepNumberNewton(1E-6,zeros(1,dimension+1));
  fig2 = figure();
  plot(e,ie);
  title("Newton, itearation(-log(eps))");
  if isSeaprate 
    print(fig2,"newtonTaskASeparated.png");
  else
    print(fig2,"newtonTaskANotSeparated.png");
  endif;

  [w,i,e,ie] = goodGradient(@fQ,@fQGrad,1E-3,1E-10,
                    0*ones(1,dimension+1));
  fig = figure();
  plot(e,ie);
  title("Gradient descent, itearation(-log(eps))");
  if isSeaprate 
    print(fig,"gradDescentTaskASeaparated.png");
  else  
    print(fig,"gradDescentTaskANotSeaparated.png");
  endif
 

  runMovingContol();
endfunction;

disp("Can separate classes");
for i=1:halfn
  fileX(i,1:dimension)=rand(1,dimension);
  fileX(i+halfn,1:dimension)=rand(1,dimension)'+2;
endfor;
fileY(1,1:halfn)*=-1;  
work(true);
disp("Can not separate classes");
for i=1:halfn
  fileX(i,1:dimension)=rand(1,dimension);
  fileX(i+halfn,1:dimension)=rand(1,dimension)';
endfor;
work(false);



