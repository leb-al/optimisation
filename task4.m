dimension = 2;

function [result] = F(w)
  result = (1-w(1))^2+100*(w(2)-(w(1)^2))^2;  
endfunction

function [gradient] = FGrad(w,x,y)
   gradient = [(-2*(1-w(1))-400*(w(2)-w(1)^2)*w(1)) (200*(w(2)-w(1)^2))]; 
endfunction



function[solution, iterationCount] = quasinewton(f,fGrad,start,eps)
	iterationCount = 0;
	current = start;
	notoptimal = true;	
	[a b] = size(start);
	quasihess = eye(b, b);
	while (notoptimal)
    ++iterationCount;
    grad = fGrad(current);
    fCurrent = f(current);
    next = current - (grad*(quasihess^(-1)));
    fvar = next - current;
    gradNext =  fGrad(next);
    fgv = gradNext - grad;
    nexthess = quasihess + (fgv')*(fgv)/(fgv*(fvar'))-quasihess*(fvar')*(fvar)*(quasihess')/(fvar*(quasihess')*(fvar'));
    if (norm(gradNext) < eps)
      solution = next;
      notoptimal=false;
    endif;
    quasihess = nexthess;
    current = next;
  endwhile;
endfunction;

[w, i] = quasinewton(@F, @FGrad, zeros(1,dimension) , 1E-10);
w
F(w)
i

[w, i] = gradientDescent(@F, @FGrad, 0.1*ones(1,dimension),
             1E-10, 0.001);
w
F(w)
i

function [iterationEps, logEps] = countStepNumber(eps, begin, method,
                                text, filename)
  cureps = 1;
  run = true;
  logEps=[];
  iterationEps=[];
  while(run)
    if cureps<eps
      cureps=eps;
      run = false;
    endif
    [w,i] = method(@F,@FGrad,begin,cureps,0.001);
    logEps= [logEps -log(cureps)];
    iterationEps= [iterationEps i];
    cureps/=3;
  endwhile;
  fig = figure();
  plot(logEps,iterationEps);
  title([text ", itearation(-log(eps))"]);
  print(fig,filename);
endfunction 

countStepNumber(1E-10,0.1*ones(1,dimension),@quasinewton,
         "Quasinewton","Quasinewton4.png");
countStepNumber(1E-5,0.1*ones(1,dimension),@gradientDescent,
         "GradinetDescent","GradinetDescent4.png");
