function[solution, iterationCount] = newton(fGrad,fHess,begin,eps)
	iterationCount = 0;
	current = begin;
	notoptimal = true;	
	while (notoptimal)
    ++iterationCount;
    grad = fGrad(current);
	  hess = fHess(current);
	  current = current - (grad*(hess^(-1)));  
   	if (norm(fGrad(current)) < eps)
      solution = current;
      notoptimal=false;
    endif;
  endwhile;
endfunction;
