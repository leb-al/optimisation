#task 123b
#Lebedev Aleksey, 393

global dimension = 30;

function [result] = Q(w,x,y)
  sizeArr = size(x);
  m=sizeArr(2);
  n=sizeArr(1);
  extendedX=[-ones(n,1) x];
  tmp = extendedX*w';
  tmp =-diag(y)*tmp; 
  tmp = exp(tmp)+1;
  tmp = log(tmp);
  result = ones(1,n)*tmp;
  
endfunction

function [gradient] = QGradient(w,x,y)
  sizeArr = size (x);
  m=sizeArr(2);
  n=sizeArr(1);
  extendedX=[-ones(n,1) x];
  tmp = extendedX*w';
  tmp =-diag(y)*tmp; 
  tmp = exp(tmp);
  sumElements = diag(-y)*diag(tmp)*(1\(tmp+1));
  gradient = sumElements'*extendedX;
endfunction

function [gradient] = QGessian(w,x,y)
  sizeArr = size (x);
  m=sizeArr(2);
  n=sizeArr(1);
  extendedX=[-ones(n,1) x];
  tmp = extendedX*w';
  tmp =-diag(y)*tmp; 
  tmp = exp(tmp);
  sumElements = diag(y)*diag(y)*diag(tmp);
  sumElements=sumElements*diag(1\(tmp+1))*(1\(tmp+1));
  gradient = extendedX'*diag(sumElements)*extendedX;
endfunction


input = fopen ("wdbc.data.txt", "r");
index = 1;
global fileX = [];
global fileY = [];
while (feof(input) != 1)
  [id, count, err ] = fscanf(input, "%d", 1);
  if (count != 0)  
    fscanf(input, "%c", 1);
    symbol = fscanf(input, "%c", 1);
    if symbol=='M' 
      fileY(index)=-1;
      else
      fileY(index)=1;
    endif;
    for i=1:dimension
      fscanf(input, "%c", 1);
      fileX(index,i)=fscanf(input, "%f", 1);
    end   
    ++index;
  endif;
endwhile;
fclose (input);

function res = fQ(w)
  global fileX;
  global fileY;
  res = Q(w,fileX,fileY);
endfunction

function res = fQGrad(w)
  global fileX;
  global fileY;
  res = QGradient(w,fileX,fileY);
endfunction

function res = fQGessian(w)
  global fileX;
  global fileY;
  res = QGessian(w,fileX,fileY);
endfunction

function res = classificator(x,w)
  res = sign([-1 x]*w');
endfunction

function [w,iterations,logEps,iterationEps] = goodGradient (f,
         fGrad, eps, lambda, begin)
  cureps = 1;
  run = true;
  w = begin;
  iterations=0; 
  logEps=[];
  iterationEps=[];
  while(run)
    if cureps<eps
      cureps=eps;
      run = false;
    endif
    [w,i] = gradientDescent(f,fGrad,w,cureps,lambda);
    iterations+=i;
    logEps= [logEps -log(cureps)];
    iterationEps= [iterationEps iterations];
    cureps/=3;
    lambda*=1.2;
  endwhile
endfunction

[w,i,e,ie] = goodGradient(@fQ,@fQGrad,1E-3,1E-10,
                  0*ones(1,dimension+1));
fig = figure();
plot(e,ie);
title("Gradient descent, itearation(-log(eps))");
print(fig,"gradDescent.png");

function [iterationEps, logEps] = countStepNumberNewton (eps, begin)
  cureps = 1;
  run = true;
  logEps=[];
  iterationEps=[];
  while(run)
    if cureps<eps
      cureps=eps;
      run = false;
    endif
    [w,i] = newton(@fQGrad,@fQGessian,begin,cureps);
    logEps= [logEps -log(cureps)];
    iterationEps= [iterationEps i];
    cureps/=3;
  endwhile
endfunction         

[e,ie] = countStepNumberNewton(1E-6,zeros(1,dimension+1));
fig2 = figure();
plot(e,ie);
title("Newton, itearation(-log(eps))");
print(fig2,"newton.png");

function [good,bad] = movingContol(indexes)
  global fileX;
  global fileY;
  global dimension;
  x=fileX;
  y=fileY;
  controlX=[];
  controlY=[];
  newX = [];
  newY = [];
  for i=1:size(fileY)(2)
    if(indexes(i)==1)
      newX=[newX;x(i,1:dimension)];
      newY=[newY y(i)];
    else
      controlX=[controlX;x(i,1:dimension)];
      controlY=[controlY; y(i)];
   endif;
  endfor;
  good=0;
  bad=0;
  [w,i]=newton(@fQGrad,@fQGessian,zeros(1,dimension+1),1E-5);
  %[w,i]=goodGradient(@fQ,@fQGrad,1E-2,1E-10,zeros(1,dimension+1));
  for i=1:size(controlY)
    if(controlY(i)==classificator(controlX(i,1:dimension),w))
      good+=1;
    else
      bad+=1;
    endif;  
  endfor;
  global fileX = newX;
  global fileY = newY;
  global fileX=x;
  global fileY=y;
endfunction


function runMovingContol()
  global fileY;
  parts = 5;
  sizeOfPart = floor(length(fileY) / parts);
  for i = 1:parts
    partFirst = sizeOfPart * (i - 1) + 1;
    partLast = partFirst + sizeOfPart - 1;
    config = ones(1,size(fileY)(2));
    config(partFirst:partLast)=zeros(1,sizeOfPart);
    printf("control %d\n",i);
    [g,b]=movingContol(config);
    total=(g+b)/100.0;
    printf("good %d (%f); bad %d (%f); total %d %d\n", 
            g, g/total, b, b/total, g+b, sizeOfPart);
  endfor;  
endfunction;

runMovingContol();